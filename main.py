from encryptor import encrypt_message,generate_key
from decryptor import decrypt_message 

key = generate_key()


encrypted = encrypt_message("Bonjour sale con" , key)
print(f"Message crypté: {encrypted}")

decrypted = decrypt_message(encrypted, key)
print(f"Message décrypté: {decrypted}")

