from encryptor import encrypt_message , generate_key
from decryptor import decrypt_message 


def test_encryption_decryption():
    key = generate_key()
    message = "Test Message"
    encrypted = encrypt_message(message, key)
    decrypted = decrypt_message(encrypted, key)
    assert message == decrypted