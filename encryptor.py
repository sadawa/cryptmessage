from cryptography.fernet import Fernet

def generate_key():
    return Fernet.generate_key()


def encrypt_message(message,key):
    f = Fernet(key)
    encrypted_message = f.encrypt(message.encode())
    return encrypted_message